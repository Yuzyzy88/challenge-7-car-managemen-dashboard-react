import '../App.css';
import img_service from '../images/img_service.svg';
import group_53 from '../images/group_53.svg';
import icon_complete from '../images/icon_complete.svg';
import icon_price from '../images/icon_price.svg';
import icon_24hrs from '../images/icon_24hrs.svg';
import icon_professional from '../images/icon_professional.svg';
import img_photo from '../images/img_photo.svg';
import Rate from '../images/Rate.svg';
import img_photo1 from '../images/img_photo1.svg';

import { useRef } from "react";

//import Button from '@mui/material/Button';

function Home() {
    const testimonailGallery = useRef(null)
    function scrollRight(e) {
        testimonailGallery.current.scrollTo({
            left: testimonailGallery.current.scrollLeft + 450,
            behavior: 'smooth'
        });
    }
    function scrollLeft(e){
        testimonailGallery.current.scrollTo({
            left: testimonailGallery.current.scrollLeft - 450,
            behavior: 'smooth'
        });
    }
    return (
        <>
            {/* offer */}
            <div className="container" style={{ marginTop: '7%' }} id="offer">
                <div className="row">
                    <div className="col-12 col-md-6 " style={{ paddingLeft: '40px', paddingRight: '40px' }}>
                        <img src={img_service} alt="img_service" className="img-fluid" />
                    </div>
                    <div className="col-12 col-md-5 pt-5">
                        <h1 className="Text-heading-one">
                            Best Car Rental for any kind of trip in (Lokasimu)!
                        </h1>
                        <p className="Text-binar">Sewa mobil di (Lokasimu) bersama Binar Car Rental
                            jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan
                            terbaik untuk perjalanan
                            wisata, bisnis, wedding, meeting, dll.</p>

                        <ul className="List-offer">
                            <li><img src={group_53} alt="" /> Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                            <li><img src={group_53} alt="" /> Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                            <li><img src={group_53} alt="" /> Sewa Mobil Jangka Panjang Bulanan</li>
                            <li><img src={group_53} alt="" /> Gratis Antar - Jemput Mobil di Bandara</li>
                            <li><img src={group_53} alt="" /> Layanan Airport Transfer / Drop In Out</li>
                        </ul>
                    </div>
                </div>
            </div>

            {/* why us */}
            <div id="why-us" className="container mt-3 pt-5 pb-5">
                <div className="row">
                    <div id="why-us-title" className="col">
                        <h1 className="Text-heading-one">Why Us?</h1>
                        <p className="Text-binar">Mengapa harus pilih Binar Car Rental?</p>
                    </div>
                </div>
                <div className="row row-cols-1 row-cols-md-4 g-4" style={{ paddingTop: '30px' }}>
                    <div className="col col-12 col-md-6 col-lg-3 ps-2">
                        <div className="card why-us-card pt-4">
                            <img src={icon_complete} alt="..." height="40px" style={{ width: 'min-content' }}
                                className="px-4" />
                            <div className="card-body p-4 pt-3">
                                <h1 className="Text-heading-two mb-3">Mobil Lengkap</h1>
                                <p className="Text-binar">
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col col-12 col-md-6 col-lg-3 ps-2">
                        <div className="card why-us-card pt-4">
                            <img src={icon_price} alt="..." height="40px" style={{ width: 'min-content' }} className="px-4" />
                            <div className="card-body p-4 pt-3">
                                <h1 className="Text-heading-two mb-3">Harga Murah</h1>
                                <p className="Text-binar">Harga murah dan bersaing, bisa bandingkan harga kami dengan
                                    rental mobil lain</p>
                            </div>
                        </div>
                    </div>
                    <div className="col col-12 col-md-6 col-lg-3 ps-2">
                        <div className="card why-us-card pt-4">
                            <img src={icon_24hrs} alt="..." height="40px" style={{ width: 'min-content' }} className="px-4" />
                            <div className="card-body p-4 pt-3">
                                <h1 className="Text-heading-two mb-3">Layanan 24 Jam</h1>
                                <p className="Text-binar">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                                    tersedia di akhir minggu</p>
                            </div>
                        </div>
                    </div>
                    <div className="col col-12 col-md-6 col-lg-3 ps-2">
                        <div className="card why-us-card pt-4">
                            <img src={icon_professional} alt="..." height="40px" style={{ width: 'min-content' }}
                                className="px-4" />
                            <div className="card-body p-4 pt-3">
                                <h1 className="Text-heading-two mb-3">Sopir Profesional</h1>
                                <p className="Text-binar">Sopir yang profesional, berpengalaman, jujur, ramah dan
                                    selalu tepat waktu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* testimonial */}
            <div className="container-fluid" id="testimonial">

                <div className="pt-5" style={{ textAlign: 'center' }}>
                    <h1 className="Text-heading">Testimonial</h1>
                    <p>Berbagai review positif dari para pelanggan kami</p>
                </div>

                <div className="row mb-3 flex-nowrap overflow-hidden" ref={testimonailGallery}>

                    <div className="card mx-3" style={{ maxWidth: '619px', minHeight: '270px', background: '#F1F3FF', borderRadius: '8px' }}>
                        <div className="row g-0">
                            <div className="col-2">
                                <img src={img_photo} className="img-fluid rounded-start mx-4" alt="..."
                                    style={{ paddingTop: '96px' }} />
                            </div>

                            <div className="col-10">
                                <div className="card-body pt-5 my-3 mx-3" style={{ width: 'fit-content' }}>
                                    <img className="pb-2" src={Rate} alt="" />
                                    <p className="Text-binar" style={{ width: '100%' }}>“Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit, sed
                                        do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”.</p>
                                    <p className="Card-text"><small className="Text-sub-heading">John Dee 32, Bromo</small></p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="card mx-3" style={{ maxWidth: '619px', minHeight: '270px', background: '#F1F3FF', borderRadius: '8px' }}>
                        <div className="row g-0">
                            <div className="col-2">
                                <img src={img_photo1} className="img-fluid rounded-start mx-4" alt="..."
                                    style={{ paddingTop: '96px' }} />
                            </div>
                            <div className="col-10">
                                <div className="card-body pt-5 my-3 mx-3" style={{ width: 'fit-content' }}>
                                    <img className="pb-2" src={Rate} alt="" />
                                    <p className="Text-binar" style={{ width: '100%' }}>“Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit, sed
                                        do
                                        eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”.</p>
                                    <p className="Card-text"><small className="Text-sub-heading">John Dee 32, Bromo</small></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="card mx-3" style={{ maxWidth: '619px', minHeight: '270px', background: '#F1F3FF', borderRadius: '8px' }}>
                        <div className="row g-0">
                            <div className="col-2">
                                <img src={img_photo} className="img-fluid rounded-start mx-4" alt="..."
                                    style={{ paddingTop: '96px' }} />
                            </div>
                            <div className="col-10">
                                <div className="card-body pt-5 my-3 mx-3" style={{ width: 'fit-content' }}>
                                    <img className="pb-2" src={Rate} alt="" />
                                    <p className="Text-binar" style={{ width: '100%' }}>“Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit, sed
                                        do
                                        eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”.</p>
                                    <p className="Card-text"><small className="Text-sub-heading">John Dee 32, Bromo</small></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="card mx-3" style={{ maxWidth: '619px', minHeight: '270px', background: '#F1F3FF', borderRadius: '8px' }}>
                        <div className="row g-0">
                            <div className="col-2">
                                <img src={img_photo1} className="img-fluid rounded-start mx-4" alt="..."
                                    style={{ paddingTop: '96px' }} />
                            </div>
                            <div className="col-10">
                                <div className="card-body pt-5 my-3 mx-3" style={{ width: 'fit-content' }}>
                                    <img className="pb-2" src={Rate} alt="" />
                                    <p className="Text-binar" style={{ width: '100%' }}>“Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit, sed
                                        do
                                        eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”.</p>
                                    <p className="Card-text"><small className="Text-sub-heading">John Dee 32, Bromo</small></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="card mx-3" style={{ maxWidth: '619px', minHeight: '270px', background: '#F1F3FF', borderRadius: '8px' }}>
                        <div className="row g-0">
                            <div className="col-2">
                                <img src={img_photo} className="img-fluid rounded-start mx-4" alt="..."
                                    style={{ paddingTop: '96px' }} />
                            </div>
                            <div className="col-10">
                                <div className="card-body pt-5 my-3 mx-3" style={{ width: 'fit-content' }}>
                                    <img className="pb-2" src={Rate} alt="" />
                                    <p className="Text-binar" style={{ width: '100%' }}>“Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit, sed
                                        do
                                        eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”.</p>
                                    <p className="Card-text"><small className="Text-sub-heading">John Dee 32, Bromo</small></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="d-flex flex-nowrap justify-content-center">
                    <button type="button" className="btn btn-primary rounded-circle mx-1" onClick={scrollLeft}>
                        <i className="bi-chevron-left" style={{ fontSize: '.9rem', color: 'white' }}></i>
                    </button>
                    <button type="button" className="btn btn-primary rounded-circle mx-1" onClick={scrollRight}>
                        <i className="bi-chevron-right" style={{ fontSize: '.9rem', color: 'white' }}></i>
                    </button>
                </div>
            </div>

            {/* banner two */}
            <div className="container d-flex justify-content-center" style={{ marginTop: '90px' }}>
                <div className="d-flex justify-content-center flex-wrap align-items-start"
                    style={{ backgroundColor: ' #0D28A6', borderRadius: '13px', maxWidth: '1168px' }}>
                    <h1 className="col-12 Text-heading-binar mt-5 mb-3" style={{ color: '#FFFFFF', textAlign: 'center' }}>Sewa Mobil di
                        (Lokasimu) Sekarang</h1>
                    <p className="col-12 Text-binar Banner-two-text"
                        style={{ marginBottom: '52px', textAlign: 'center', color: '#FFFFFF' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div style={{ flex: '1 100%' }} className="col-12 d-flex justify-content-center mb-5">
                        <button type="button" className="Btn-primary">Mulai Sewa Mobil</button>
                    </div>
                </div>

            </div>

            {/* question */}
            <div id="faq" className="container" style={{ marginTop: '90px', marginBottom: '110px' }}>
                <div className="d-flex justify-content-around pt-5 flex-wrap">
                    <div className="col col-12 col-md-4">
                        <h5 className="Text-heading-one mb-3">Frequently Asked Question</h5>
                        <p className="Text-binar" style={{ width: '100%' }}>Mengapa harus pilih Binar Car Rental?</p>
                    </div>
                    <div className="col col-12 col-md-6">
                        <div className="accordion" id="accordianFAQ">
                            <div className="accordion-item">
                                <h2 className="accordion-header Text-binar" id="flush-headingOne">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapseOne" aria-expanded="false"
                                        aria-controls="flush-collapseOne">
                                        Apa saja syarat yang dibutuhkan?
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" className="accordion-collapse collapse"
                                    aria-labelledby="flush-headingOne" data-bs-parent="#accordianFAQ">
                                    <div className="accordion-body">Placeholder content for this accordion, which is intended to
                                        demonstrate the <code>.accordion-flush</code> className. This is the first item's accordion
                                        body.</div>
                                </div>
                            </div>
                            <div className="accordion-item mt-3">
                                <h2 className="accordion-header" id="flush-headingTwo">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                        aria-controls="flush-collapseTwo">
                                        Berapa hari minimal sewa mobil lepas kunci?
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" className="accordion-collapse collapse"
                                    aria-labelledby="flush-headingTwo" data-bs-parent="#accordianFAQ">
                                    <div className="accordion-body">Placeholder content for this accordion, which is intended to
                                        demonstrate the <code>.accordion-flush</code> className. This is the second item's accordion
                                        body. Let's imagine this being filled with some actual content.</div>
                                </div>
                            </div>
                            <div className="accordion-item mt-3">
                                <h2 className="accordion-header" id="flush-headingThree">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapseThree" aria-expanded="false"
                                        aria-controls="flush-collapseThree">
                                        Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" className="accordion-collapse collapse"
                                    aria-labelledby="flush-headingThree" data-bs-parent="#accordianFAQ">
                                    <div className="accordion-body">Placeholder content for this accordion, which is intended to
                                        demonstrate the <code>.accordion-flush</code> className. This is the third item's accordion
                                        body. Nothing more exciting happening here in terms of content, but just filling up the
                                        space to make it look, at least at first glance, a bit more representative of how this
                                        would look in a real-world application.</div>
                                </div>
                            </div>
                            <div className="accordion-item mt-3">
                                <h2 className="accordion-header" id="flush-headingFour">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapseFour" aria-expanded="false"
                                        aria-controls="flush-collapseFour">
                                        Apakah Ada biaya antar-jemput?
                                    </button>
                                </h2>
                                <div id="flush-collapseFour" className="accordion-collapse collapse"
                                    aria-labelledby="flush-headingFour" data-bs-parent="#accordianFAQ">
                                    <div className="accordion-body">Placeholder content for this accordion, which is intended to
                                        demonstrate the <code>.accordion-flush</code> className. This is the third item's accordion
                                        body. Nothing more exciting happening here in terms of content, but just filling up the
                                        space to make it look, at least at first glance, a bit more representative of how this
                                        would look in a real-world application.</div>
                                </div>
                            </div>
                            <div className="accordion-item mt-3">
                                <h2 className="accordion-header" id="flush-headingFive">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapseFive" aria-expanded="false"
                                        aria-controls="flush-collapseFive">
                                        Bagaimana jika terjadi kecelakaan
                                    </button>
                                </h2>
                                <div id="flush-collapseFive" className="accordion-collapse collapse"
                                    aria-labelledby="flush-headingFive" data-bs-parent="#accordianFAQ">
                                    <div className="accordion-body">Placeholder content for this accordion, which is intended to
                                        demonstrate the <code>.accordion-flush</code> className. This is the third item's accordion
                                        body. Nothing more exciting happening here in terms of content, but just filling up the
                                        space to make it look, at least at first glance, a bit more representative of how this
                                        would look in a real-world application.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        </>

    )

}
export default Home;