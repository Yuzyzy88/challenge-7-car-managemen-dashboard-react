import { useEffect, useState } from 'react';
import '../App.css';
import CarCard from "../components/CarCard";
import { useDispatch, getState } from "react-redux";
import { fetchCars } from '../features/CarSlice';
function Cars() {
    const dispatch  = useDispatch();
    const [allCars, setAllCars] = useState([])
    const [filteredCars, setFilteredCars] = useState([])

    useEffect(() => {
        if (allCars.length === 0) {
            dispatch(fetchCars()).unwrap().then((result) => {
                setAllCars(result)
            })
        }
    })

    const [capacity, setCapacity] = useState('')
    const [carDate, setcarDate] = useState('2020-05-29')
    const [carTime, setcarTime] = useState('08:00 WIB')

    const findCar = async () => {
        // change date and time to be epochtime
        const datetime = new Date(`${carDate} ${carTime.substr(0, 5)}`)
        const beforeEpochTime = datetime.getTime()

        const filteredCars = await allCars.filter((item) => {
            // Conditional (Ternary) Operator to filter capacity
            const capacity_filter = capacity != '' && capacity > 0 ? item.capacity >= capacity : true;

            // filter date time 
            const itemDate = new Date(item.availableAt);
            const date_filter = itemDate.getTime() <= beforeEpochTime;

            return capacity_filter && date_filter;
        });

        setFilteredCars(filteredCars)
        console.log(filteredCars)
    }


    return (
        <>
            {/* filter form  */}
            {/* <form method="GET"> */}
                <div className="search-card">
                    <div className="d-flex justify-content-center">
                        <div className="card " style={{ width: '1130px', minHeight: '106px' }}>
                            <div className="row row-cols-1 row-cols-md-4 g-4 px-4 pt-4 pb-4">

                                <div className="col col-12 col-md-6 col-lg-2" style={{ width: '230px' }}>
                                    <p className="Text-binar mb-1">Tipe Driver</p>
                                    <div className="dropdown">
                                        <select name="form_driver"
                                            style={{ backgroundColor: 'white', width: '210px', height: '36px', textAlign: 'left' }}>
                                            <option value="">Pilih Driver</option>
                                            <option value="Dengan supir">Dengan supir</option>
                                            <option value="Lepas Kunci">Lepas Kunci</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="col col-12 col-md-6 col-lg-2" style={{ width: '230px' }}>
                                    <p className="Text-binar mb-1">Tanggal</p>
                                    <div className="input-group date" id="datepicker" style={{ width: '210px', height: '36px' }}>
                                        <input type="date" id="form_date" name="form_date" value={carDate} onChange={e => setcarDate(e.target.value)} className="form-control"
                                            placeholder="Pilih Tanggal" />
                                        <span className="input-group-append">
                                            <span className="input-group-text bg-white d-block">
                                                <i className="fa fa-calendar"></i>
                                            </span>
                                        </span>
                                    </div>

                                </div>

                                <div className="col col-12 col-md-6 col-lg-2" style={{ width: '230px' }}>
                                    <p className="Text-binar mb-1">Waktu Jemput/Ambil</p>
                                    <select id="form_time" name="form_time" value={carTime} onChange={e => setcarTime(e.target.value)} className="icon-time"
                                        style={{ width: '210px', height: '36px' }}>
                                        <option value="08:00 WIB">08:00 WIB</option>
                                        <option value="09:00 WIB">09:00 WIB</option>
                                        <option value="10:00 WIB">10:00 WIB</option>
                                        <option value="11:00 WIB">11:00 WIB</option>
                                        <option value="12:00 WIB">12:00 WIB</option>
                                        <option value="13:00 WIB">13:00 WIB</option>
                                        <option value="14:00 WIB">14:00 WIB</option>
                                        <option value="15:00 WIB">15:00 WIB</option>
                                        <option value="16:00 WIB">16:00 WIB</option>
                                        <option value="17:00 WIB">17:00 WIB</option>
                                        <option value="18:00 WIB">18:00 WIB</option>
                                    </select>
                                </div>

                                <div className="col col-12 col-md-6 col-lg-2" style={{ width: '230px' }}>
                                    <p className="Text-binar mb-1">Jumlah Penumpang (optional)</p>
                                    <input type="number" name="form_capacity" id="form_capacity" className="icon-user"
                                        value={capacity} onChange={e => setCapacity(e.target.value)} placeholder="Jumlah Penumpang" style={{ width: '210px', height: '36px' }} />
                                </div>

                                <div className="col col-12 col-md-6 col-lg-2">
                                    <button className="Btn-primary mt-3" onClick={findCar}>Cari Mobil</button>
                                    <button id="clear-btn">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {/* </form> */}

            {/* catalog car */}
            <div className="container" style={{ marginTop: '8%', marginBottom: '4%' }}>
                <div className="row">
                    <div className='d-flex flex-wrap'>
                        {
                            filteredCars.map((car, key) => (
                                <CarCard car={car} key={key}/>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default Cars;