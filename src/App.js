import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Home from './views/Home';
import Header from './components/Header';
import Footer from './components/Footer';
import Cars from './views/Cars';



function App() {
  return (
    <div>
      <Header />
      <BrowserRouter basename='/'>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/cars" element={<Cars />} />          
        </Routes>
      </BrowserRouter>
      <Footer />
    </div>
  );
}

export default App;
