import '../App.css';
import logo from '../images/logo.svg';
import icon_facebook from '../images/icon_facebook.svg';
import icon_instagram from '../images/icon_instagram.svg';
import icon_twitter from '../images/icon_twitter.svg';
import icon_mail from '../images/icon_mail.svg';
import icon_twitch from '../images/icon_twitch.svg';

function Footer() {
    return (

        // footer
        <div id="footer" className="mb-5">
            <div className="d-flex justify-content-center text-md-start px-3">
                <div className="row" style={{ width: '100%', maxWidth: '1141px' }}>
                    <div className="col-12 col-md-3">
                        <p className="Text-binar">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p className="Text-binar">binarcarrental@gmail.com</p>
                        <p className="Text-binar">081-233-334-808</p>
                    </div>
                    <div className="col-12 col-md-3">
                        <ul className="list-offer">
                            <li style={{ marginBottom: '10px' }}><a href="#offer" className="Text-footer">Our Service</a></li>
                            <li style={{ marginBottom: '10px' }}><a href="#why-us" className="Text-footer">Why US</a></li>
                            <li style={{ marginBottom: '10px' }}><a href="#testimonial" className="Text-footer">Testimonial</a></li>
                            <li style={{ marginBottom: '10px' }}><a href="#faq" className="Text-footer">FAQ</a></li>
                        </ul>
                    </div>
                    <div className="col-12 col-md-3 mb-3">
                        <p className="Text-binar">Connect with us</p>
                        <img className="px-1 px-md-0 px-lg-1" src={icon_facebook} alt="" style={{ width: 'min-content' }} />
                        <img className="px-1 px-md-0 px-lg-1" src={icon_instagram} alt="" style={{ width: 'min-content' }} />
                        <img className="px-1 px-md-0 px-lg-1" src={icon_twitter} alt="" style={{ width: 'min-content' }} />
                        <img className="px-1 px-md-0 px-lg-1" src={icon_mail} alt="" style={{ width: 'min-content' }} />
                        <img className="px-1 px-md-0 px-lg-1" src={icon_twitch} alt="" style={{ width: 'min-content' }} />
                    </div>
                    <div className="col-12 col-md-3 mb-5 mb-md-0">
                        <p className="Text-binar">Copyright Binar 2022</p>
                        <img src={logo} alt="" />
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Footer;