import '../App.css';
import fi_users from '../images/fi_users.svg';
import fi_settings from '../images/fi_settings.svg';
import fi_calendar from '../images/fi_calendar.svg';

function CarCard({ car }) {
    return (
        <div className="col-12 col-md-6 col-lg-4">
            <div className="card List-car ">
                <div className="p-4 " style={{ minHeight: '725px' }}>
                    <img src={car.image.substr(1)} alt={car.manufacture} width='100%' style={{ height: '300px' }} />
                    <p className="Title-filter pt-3">{car.type} / {car.model}</p>
                    <p className="Text-heading-two">Rp{car.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} / hari</p>
                    <p className="Text-binar">{car.description}</p>
                    <p className="Text-binar"><img src={fi_users} alt='' /> {car.capacity} Orang</p>
                    <p className="Text-binar"><img src={fi_settings} alt='' /> {car.transmission}</p>
                    <p className="Text-binar"><img src={fi_calendar} alt='' /> Tahun {car.year}</p>
                    <p>{car.availableAt}</p>
                </div>
                <div style={{ textAlign: 'center' }} className="mb-3">
                    <button className="Btn-primary" style={{ width: '87%' }}>Pilih Mobil</button>
                </div>
            </div>
        </div>
    )
}

export default CarCard;