import '../App.css';
import logo from '../images/logo.svg';
import car from '../images/img_car.svg';

function Header() {
    return (
        <div>
            {/* navbar mobile */}
            <div className="container-fuild d-block d-md-none "
                style={{ backgroundColor: '#f1f3ff', position: 'fixed', width: '100%', zIndex: '1' }}>
                <div className="d-flex justify-content-between align-center pt-4 px-3">
                    <div className="col-auto">
                        <img src={logo} alt="logo" />
                    </div>
                    <div className="col-auto">
                        <input type="checkbox" name="menu-toggle" id="menu-toggle" className="d-none" />
                        <label for="menu-toggle">
                            <i className="bi-list" style={{ fontSize: '1.5rem' }}></i>
                        </label>
                        <div className="mobile-menu overflow-hidden bg-white align-start justify-content-end"
                            style={{ position: 'fixed', height: '100vh', top: '0', zIndex: '1' }}>
                            <nav className="justify-content-start px-3" style={{ position: 'relative', width: '180px' }}>
                                <ul className="list-unstyled">
                                    <li className="d-flex justify-content-between align-center mt-5">
                                        <span className="py-2">BCR</span>

                                        <label for="menu-toggle">
                                            <i className="bi-x" style={{ fontSize: '1.5rem' }}></i>
                                        </label>
                                    </li>
                                    <li><a href="#offer" className="Text-footer">Our Service</a></li>
                                    <li><a href="#why-us" className="Text-footer">Why Us</a></li>
                                    <li><a href="#testimonial" className="Text-footer">Testimonial</a></li>
                                    <li><a href="#faq" className="Text-footer">FAQ</a></li>
                                    <li><button type="button" className="Btn-primary">Register</button></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>


            {/* navbar desktop  */}
            <div className='container-fuild d-none d-md-block'
                style={{ backgroundColor: '#f1f3ff', position: 'fixed', width: '100%', zIndex: '1' }}>
                <div className='container'>
                    <nav className='navbar navbar-expand-md navbar-light justify-content-between'>
                        <img src={logo} alt="logo" />
                        <div>
                            <ul className="navbar-nav ms-auto text-end">
                                <li><a href="#offer" className="nav-link px-3">Our Service</a></li>
                                <li><a href="#why-us" className="nav-link px-3">Why Us</a></li>
                                <li><a href="#testimonial" className="nav-link px-3">Testimonial</a></li>
                                <li><a href="#faq" className="nav-link px-3">FAQ</a></li>
                                <li><button className="Btn-primary">Register</button></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

            {/* banner */}
            <div className="container-fluid" style={{ backgroundColor: '#f1f3ff', paddingTop: '60px' }}>
                <div className="row align-items-center justify-content-center">
                    <div id="card-title" className="col-12 col-md-5 col-lg-6 pt-3">
                        <h1 className="Text-heading-binar">
                            Sewa & Rental Mobil Terbaik di
                            kawasan (Lokasimu)
                        </h1>
                        <p id="card-text-title" className="Text-binar">
                            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
                            untuk sewa mobil selama 24 jam.
                        </p>
                        <button className="Btn-primary"><a href="/cars" className="Text-btn"> Mulai Sewa Mobil</a></button>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 pt-4 align-self-end" data-aos="fade-down" data-aos-duration="1000">
                        <img src={car} alt="" className="img-fluid" style={{ width: '100%' }} />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Header;