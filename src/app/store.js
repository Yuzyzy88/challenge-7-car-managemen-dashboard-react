import { configureStore } from "@reduxjs/toolkit";
import carReducer from '../features/CarSlice';

export default configureStore({
    reducer: {
        cars: carReducer
    },
    devTools: true
})