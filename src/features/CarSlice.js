import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"

export const fetchCars = createAsyncThunk('cars/fetchCars', async (args, thunkAPI) => {
    const response = await fetch('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
    const data = await response.json()
    return data
})

export const carSlice = createSlice({
    name: 'car',
    initialState: {
        data: [],
        status: 'idle',
        error: null
    },
    reducers: {},
    extraReducers: {
        [fetchCars.pending]: (state, action) => {
            state.status = 'loading'
        },
        [fetchCars.fulfilled]: (state, action) => {
            state.status = 'success'
            state.data = action.payload
        },
        [fetchCars.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message;
        }
    }
})

export const getAllCars = (state) => state.car.data
export default carSlice.reducer